window.onload = () => {

  const door1 = document.getElementById('js--door');
  const door2 = document.getElementById('js--door2');
  const door3 = document.getElementById('js--door3');
  const door4 = document.getElementById('js--door4');
  const camera = document.getElementById('js--camera');
  const scene = document.getElementById('js--scene');
  const blus_items = document.getElementsByClassName("js--blussen");
  const brandblusser_keuken_straal = document.getElementById('js--blussen_straal');
  const cursor = document.getElementById('js--cursor');
  const straalBlussen = document.getElementById('js--straalBlussen');
  const brandblusser_keuken = document.getElementById('js--brandblusser--keuken');

  var hold = null;

  // Set default particle states
  straalBlussen.setAttribute("visible", 'false')

  door1.addEventListener('click', function(evt){
    door1.setAttribute("class", "")
    door1.setAttribute("src", "#door_inverted")
  });

  door2.addEventListener('click', function(evt){
    door2.setAttribute("class", "")
    door2.setAttribute("src", "#door_inverted")
  });

  door3.addEventListener('click', function(evt){
    door3.setAttribute("class", "")
    door3.setAttribute("src", "#door")
  });

  door4.addEventListener('click', function(evt){
    door4.setAttribute("class", "")
    door4.setAttribute("src", "#door")
  });

  brandblusser_keuken.addEventListener("click", function(evt){
    if (hold == null){
      hold = "brandblusser";
      blussen()
      camera.innerHTML += '<a-box id="js--hold" class="js--pickup" rotation="0 -90 0" scale="1.5 1.5 1.5" gltf-model="#brandblusser-glb" position="0.5 -0.5 -0.5"></a-box>';
      this.remove();
    }
  });

  const blussen = () => {
    console.log("blussen()");
    straalBlussen.setAttribute("visible", 'true')
  };
  
}
